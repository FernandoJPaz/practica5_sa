const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const request = require('request');
var rp = require('request-promise');

//Index
app.get('/', function (req, res) {
    res.send('Saludos desde Servidor ESB');

});

//Cliente Envia Orden al restaurante
app.post('/postorden',body_parser, function(req,res){

    //Orquestador Envia Orden al Restaurante
    var orden = req.body.idorden
    request.post('http://localhost:3000/postorden',{
        json:{
            'idorden':orden
        }
    })

    res.send("OK")
});

//Estado del pedido en el restaurante
app.get('/estado/:orden',body_parser, function(req,res){

    //Orquestador Envia Orden al Restaurante
    var orden = req.params.orden

    rp.get('http://localhost:3000/estado/' + orden)
    .then(function(response){
        
        res.json(response)

    })
    .catch(function (err) {
        console.log(err)
    });

});

//Restaurante Envia la orden a los repartidores
app.post('/orden/Repartidor',body_parser,function(req,res){

    var orden = req.body.idorden

    rp.post('http://localhost:3002/orden',{
        json:{
            'idorden': orden
        }
    })
    .then(function(response){
        
        res.json(response)

    })
    .catch(function (err) {
        console.log(err)
    });

    
});

//Estado del pedido en el repartidor  
app.get('/repartidor/estado/:orden',body_parser, function(req,res){

    //Orquestador Envia Orden al Restaurante
    var orden = req.params.orden

    rp.get('http://localhost:3002/estado/' + orden)
    .then(function(response){
        
        res.json(response)

    })
    .catch(function (err) {
        console.log(err)
    });

});

//.LOG
app.post('/log',body_parser,function(req,res){
    var descripcion = req.body.descripcion
    
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    res.send("OK")
});




app.listen(3010, () => {
    console.log("El servidor Log está inicializado en el puerto 3010");
   });
