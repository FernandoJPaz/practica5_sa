const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const fs = require('fs');

var First  = true ;

//Index
app.get('/', function (req, res) {
    res.send('Saludos desde Servidor Log');

});

//Agregar Log
app.post('/log',body_parser,function(req, res){

    var descripcion = req.body.descripcion;
    var fecha = new Date();
    var line = "log: "+descripcion + " "+fecha + "\n"
    var logger = fs.createWriteStream('server.log', {
        flags: 'a'
    })
    logger.write(line)
    logger.end()
    res.send("OK")
});

//Servidor run en puerto 3004
app.listen(3004, () => {
 console.log("El servidor Log está inicializado en el puerto 3004");
});

